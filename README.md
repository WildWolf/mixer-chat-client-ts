# Mixer Chat Client TypeScript

This is a simple, bare-bones, and fast typescript chat client for Mixer.com.

It works on both front-end and back-end typescript/node projects.

Its javascript equivalent is currently in use powering every chat connection over at https://pixel.chat and can be found [here](https://www.npmjs.com/package/mixer-chat-client)

## Installation:

With npm:

```bash
npm i mixer-chat-client --save
```

## Usage:

```ts
import ChatSocket from "mixer-chat-client-ts"

// You will have to get the correct chat server yourself from the mixer API

//Arguments are: Chat server URI, channelID, userID(optional), authToken(optional)
let Chat = new ChatSocket("wss://chat.mixer.com:443", 60302, 85878, "asdf1234");

Chat.on("connect", () => {
  console.log("connected");
});

Chat.on("welcome", () => {
  console.log("Recieved welcome event, shutting down");
  Chat.close();
});

Chat.connect();
```

## Events:

#### msg, whisper
```ts
on(event: 'msg', cb: (message: IMessage) => any);
on(event: 'whisper', cb: (message: IMessage) => any);

interface IMessage{
    message: IMessageData[];
    username: string;
    level: number;
    roles: string[];
    chatId: string;
    userId: number;
    meta: IMeta;
    chatLevel: number;
}

interface IMessageData {
    type: string;
    text: string;
    data?: string;
    source?: string;
    pack?: string;
    coords?: {
        x: number;
        y: number;
        width: number;
        height: number;
    };
    url?: string;
}

interface IMeta {
    is_skill?: boolean;
    skill?: {
        skill_id: string;
        skill_name: string;
        execution_id: string;
        icon_url: string;
        cost: number;
        currency: string;
    };
    censored?: boolean;
    me?: boolean;
    whisper?: boolean;
}
```

#### userJoin, userLeave
```ts
on(event: 'userJoin', cb: (message: IUser) => any);
on(event: 'userLeave', cb: (message: IUser) => any); //does not inlcude with roles

interface IUser {
    username: string;
    roles?: string[];
    channel: number;
    id: number;
}
```

#### purge

```ts
on(event: 'purge', cb: (message: IPurge) => any);

interface IPurge {
    userId: number;
}
```

#### delete

```ts
on(event: 'delete', cb: (message: IDelete) => any);

interface IDelete {
    chatId: string;
}
```

#### clear, welcome

```ts
on(event: 'clear', cb: () => any);
on(event: 'welcome', cb: () => any);
```

#### pollStart, pollEnd

```ts
on(event: 'pollStart', cb: (message: IPoll) => any): this;
on(event: 'pollEnd', cb: (message: IPoll) => any): this;

interface IPoll {
    username: string;
    userId: number;
    question: string;
    answers: { [id:string]:number };
}
```

## Methods:

```ts
connect();
close():Promise<void>
send(type: string, method: string, args: any[] = []): Promise<void>
```