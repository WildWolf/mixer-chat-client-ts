/// <reference types="node" />
import { EventEmitter } from "events";
export declare class ChatSocket extends EventEmitter {
    url: string;
    channelId: number;
    userId: number;
    authKey: string;
    id: number;
    reconnect: boolean;
    connected: boolean;
    lastPing: number;
    reconInterval: number | NodeJS.Timer;
    pingInt: number | NodeJS.Timer;
    socket: WebSocket;
    constructor(url: string, channelId: number, userId?: number, authKey?: string);
    private onMessage;
    private onClose;
    private onError;
    private ping;
    private onConnect;
    connect(): void;
    send(type: string, method: string, args?: any[]): Promise<void>;
    close(): Promise<void>;
    on(event: 'msg', cb: (message: IMessage) => any): this;
    on(event: 'whisper', cb: (message: IMessage) => any): this;
    on(event: 'clear', cb: () => any): this;
    on(event: 'purge', cb: (message: IPurge) => any): this;
    on(event: 'delete', cb: (message: IDelete) => any): this;
    on(event: 'userJoin', cb: (message: IUser) => any): this;
    on(event: 'userLeave', cb: (message: IUser) => any): this;
    on(event: 'pollStart', cb: (message: IPoll) => any): this;
    on(event: 'pollEnd', cb: (message: IPoll) => any): this;
    on(event: 'welcome', cb: () => any): this;
}
export interface IMessage {
    message: IMessageData[];
    username: string;
    level: number;
    roles: string[];
    chatId: string;
    userId: number;
    meta: IMeta;
    chatLevel: number;
}
export interface IMessageData {
    type: string;
    text: string;
    data?: string;
    source?: string;
    pack?: string;
    coords?: {
        x: number;
        y: number;
        width: number;
        height: number;
    };
    url?: string;
}
export interface IMeta {
    is_skill?: boolean;
    skill?: {
        skill_id: string;
        skill_name: string;
        execution_id: string;
        icon_url: string;
        cost: number;
        currency: string;
    };
    censored?: boolean;
    me?: boolean;
    whisper?: boolean;
}
export interface IPurge {
    userId: number;
}
export interface IDelete {
    chatId: string;
}
export interface IUser {
    username: string;
    roles?: string[];
    channel: number;
    id: number;
}
export interface IPoll {
    username: string;
    userId: number;
    question: string;
    answers: {
        [id: string]: number;
    };
}
