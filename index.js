"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const events_1 = require("events");
let WS;
if (typeof window === "undefined")
    WS = require("ws");
else
    WS = window.WebSocket;
class ChatSocket extends events_1.EventEmitter {
    constructor(url, channelId, userId = null, authKey = null) {
        super();
        this.url = url;
        this.channelId = channelId;
        this.userId = userId;
        this.authKey = authKey;
        this.onMessage = this.onMessage.bind(this);
        this.onClose = this.onClose.bind(this);
        this.onError = this.onError.bind(this);
        this.onConnect = this.onConnect.bind(this);
        this.id = 0;
        this.reconnect = true;
        this.connected = false;
        this.lastPing = 0;
    }
    onMessage(e) {
        let data = JSON.parse(e.data);
        let evt = {};
        switch (data.event) {
            case "ChatMessage":
                evt = {
                    message: data.data.message.message,
                    username: data.data.user_name,
                    level: data.data.user_level,
                    roles: data.data.user_roles,
                    chatId: data.data.id,
                    userId: data.data.user_id,
                    meta: data.data.message.meta,
                    chatLevel: data.data.user_ascension_level,
                };
                if (data.data.message.meta.whisper === true)
                    this.emit("whisper", evt);
                else
                    this.emit("msg", evt);
                break;
            case "SkillAttribution":
                evt = {
                    userId: data.data.user_id,
                    username: data.data.user_name,
                    cost: data.data.skill.cost,
                    currency: data.data.skill.currency,
                    message: typeof data.data.message === "undefined"
                        ? []
                        : data.data.message.message,
                    meta: typeof data.data.message === "undefined"
                        ? null
                        : data.data.message.meta
                };
                this.emit("skill", evt);
                break;
            case "ClearMessages":
                this.emit("clear");
                break;
            case "PurgeMessage":
                evt = {
                    userId: data.data.user_id
                };
                this.emit("purge", evt);
                break;
            case "DeleteMessage":
                evt = {
                    chatId: data.data.id
                };
                this.emit("delete", evt);
                break;
            case "UserJoin":
                evt = {
                    id: data.data.id,
                    username: data.data.username,
                    roles: data.data.roles,
                    channel: data.data.channel
                };
                this.emit("userJoin", evt);
                break;
            case "UserLeave":
                evt = {
                    id: data.data.id,
                    username: data.data.username,
                    channel: data.data.channel
                };
                this.emit("userLeave", evt);
                break;
            case "PollStart":
                evt.username = data.data.author.user_name;
                evt.userId = data.data.author.user_id;
                evt.question = data.data.q;
                evt.answers = data.data.responses;
                this.emit("pollStart", evt);
                break;
            case "PollEnd":
                evt.username = data.data.author.user_name;
                evt.userId = data.data.author.user_id;
                evt.question = data.data.q;
                evt.answers = data.data.responses;
                this.emit("pollEnd", evt);
                break;
            case "WelcomeEvent":
                this.emit("welcome");
                break;
        }
    }
    onClose(e) {
        this.connected = false;
        clearInterval(this.pingInt);
        if (this.reconnect)
            this.reconInterval = setTimeout(() => {
                this.connect();
            }, 5000);
    }
    onError(e) { }
    ping() {
        this.send("method", "ping");
    }
    onConnect(e) {
        if (this.authKey !== null && this.userId !== null)
            this.send("method", "auth", [this.channelId, this.userId, this.authKey]);
        else
            this.send("method", "auth", [this.channelId]);
        this.emit("connect");
    }
    connect() {
        this.socket = new WS(this.url);
        this.socket.onmessage = this.onMessage;
        this.socket.onclose = this.onClose;
        this.socket.onerror = this.onError;
        this.socket.onopen = this.onConnect;
        this.pingInt = setInterval(() => {
            this.ping();
        }, 15000);
    }
    send(type, method, args = []) {
        return new Promise((resolve, reject) => {
            let toSend = {
                type: type,
                method: method,
                arguments: args,
                id: this.id++
            };
            this.socket.send(JSON.stringify(toSend));
            resolve();
        });
    }
    close() {
        return new Promise((resolve, reject) => {
            this.reconnect = false;
            clearInterval(this.pingInt);
            this.socket.close();
            resolve();
        });
    }
    on(event, cb) {
        super.on(event, cb);
        return this;
    }
}
exports.ChatSocket = ChatSocket;
;
//# sourceMappingURL=index.js.map