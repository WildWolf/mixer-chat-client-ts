import { EventEmitter } from "events";

let WS;

if (typeof window === "undefined") WS = require("ws");
else WS = window.WebSocket;

export class ChatSocket extends EventEmitter {
    url: string;
    channelId: number;
    userId: number;
    authKey: string;
    id: number;
    reconnect: boolean;
    connected: boolean;
    lastPing: number;
    reconInterval: number | NodeJS.Timer;
    pingInt: number | NodeJS.Timer;
    socket: WebSocket;

    constructor(url: string, channelId: number, userId: number = null, authKey: string = null) {
        super();
        this.url = url;
        this.channelId = channelId;
        this.userId = userId;
        this.authKey = authKey;
        this.onMessage = this.onMessage.bind(this);
        this.onClose = this.onClose.bind(this);
        this.onError = this.onError.bind(this);
        this.onConnect = this.onConnect.bind(this);
        this.id = 0;
        this.reconnect = true;
        this.connected = false;
        this.lastPing = 0;
    }

    private onMessage(e) {
        let data = JSON.parse(e.data);
        let evt: any = {};
        switch (data.event) {
            case "ChatMessage":
                evt = {
                    message: data.data.message.message,
                    username: data.data.user_name,
                    level: data.data.user_level,
                    roles: data.data.user_roles,
                    chatId: data.data.id,
                    userId: data.data.user_id,
                    meta: data.data.message.meta,
                    chatLevel: data.data.user_ascension_level,
                };
                if (data.data.message.meta.whisper === true) this.emit("whisper", evt);
                else this.emit("msg", evt);
                break;
            case "SkillAttribution":
                evt = {
                    userId: data.data.user_id,
                    username: data.data.user_name,
                    cost: data.data.skill.cost,
                    currency: data.data.skill.currency,
                    message:
                        typeof data.data.message === "undefined"
                            ? []
                            : data.data.message.message,
                    meta:
                        typeof data.data.message === "undefined"
                            ? null
                            : data.data.message.meta
                };

                this.emit("skill", evt);
                break;
            case "ClearMessages":
                this.emit("clear");
                break;
            case "PurgeMessage":
                evt = {
                    userId: data.data.user_id
                };
                this.emit("purge", evt);
                break;
            case "DeleteMessage":
                evt = {
                    chatId: data.data.id
                };
                this.emit("delete", evt);
                break;
            case "UserJoin":
                evt = {
                    id: data.data.id,
                    username: data.data.username,
                    roles: data.data.roles,
                    channel: data.data.channel
                };
                this.emit("userJoin", evt);
                break;
            case "UserLeave":
                evt = {
                    id: data.data.id,
                    username: data.data.username,
                    channel: data.data.channel
                };
                this.emit("userLeave", evt);
                break;
            case "PollStart":
                evt.username = data.data.author.user_name;
                evt.userId = data.data.author.user_id;
                evt.question = data.data.q;
                evt.answers = data.data.responses;
                this.emit("pollStart", evt);
                break;
            case "PollEnd":
                evt.username = data.data.author.user_name;
                evt.userId = data.data.author.user_id;
                evt.question = data.data.q;
                evt.answers = data.data.responses;
                this.emit("pollEnd", evt);
                break;
            case "WelcomeEvent":
                this.emit("welcome");
                break;
        }
    }

    private onClose(e) {
        this.connected = false;
        clearInterval(this.pingInt as number);
        if (this.reconnect)
            this.reconInterval = setTimeout(() => {
                this.connect();
            }, 5000);
    }

    private onError(e) { }

    private ping() {
        this.send("method", "ping");
    }

    private onConnect(e) {
        if (this.authKey !== null && this.userId !== null) this.send("method", "auth", [this.channelId, this.userId, this.authKey]);
        else this.send("method", "auth", [this.channelId]);
        this.emit("connect");
    }

    connect() {
        this.socket = new WS(this.url);
        this.socket.onmessage = this.onMessage;
        this.socket.onclose = this.onClose;
        this.socket.onerror = this.onError;
        this.socket.onopen = this.onConnect;
        this.pingInt = setInterval(() => {
            this.ping();
        }, 15000);
    }

    send(type: string, method: string, args: any[] = []): Promise<void> {
        return new Promise((resolve, reject) => {
            let toSend = {
                type: type,
                method: method,
                arguments: args,
                id: this.id++
            };
            this.socket.send(JSON.stringify(toSend));
            resolve();
        });
    }

    close(): Promise<void> {
        return new Promise((resolve, reject) => {
            this.reconnect = false;
            clearInterval(this.pingInt as number);
            this.socket.close();
            resolve();
        });
    }

    on(event: 'msg', cb: (message: IMessage) => any): this;
    on(event: 'whisper', cb: (message: IMessage) => any): this;
    on(event: 'clear', cb: () => any): this;
    on(event: 'purge', cb: (message: IPurge) => any): this;
    on(event: 'delete', cb: (message: IDelete) => any): this;
    on(event: 'userJoin', cb: (message: IUser) => any): this;
    on(event: 'userLeave', cb: (message: IUser) => any): this;
    on(event: 'pollStart', cb: (message: IPoll) => any): this;
    on(event: 'pollEnd', cb: (message: IPoll) => any): this;
    on(event: 'welcome', cb: () => any): this;
    on(event: any, cb: any): this {
        super.on(event, cb);
        return this;
    }

};

export interface IMessage {
    message: IMessageData[],
    username: string,
    level: number,
    roles: string[],
    chatId: string,
    userId: number,
    meta: IMeta,
    chatLevel: number,
}

export interface IMessageData {
    type: string;
    text: string;
    data?: string;
    source?: string;
    pack?: string;
    coords?: {
        x: number;
        y: number;
        width: number;
        height: number;
    };
    url?: string;
}

export interface IMeta {
    is_skill?: boolean;
    skill?: {
        skill_id: string;
        skill_name: string;
        execution_id: string;
        icon_url: string;
        cost: number;
        currency: string;
    };
    censored?: boolean;
    me?: boolean;
    whisper?: boolean;
}

export interface IPurge {
    userId: number;
}

export interface IDelete {
    chatId: string;
}

export interface IUser {
    username: string;
    roles?: string[];
    channel: number;
    id: number;
}

export interface IPoll {
    username: string;
    userId: number;
    question: string;
    answers: { [id:string]:number };
}